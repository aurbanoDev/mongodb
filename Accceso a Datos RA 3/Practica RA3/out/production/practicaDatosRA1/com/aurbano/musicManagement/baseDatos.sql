CREATE DATABASE IF NOT EXISTS music;
use music;

CREATE TABLE IF NOT EXISTS USERS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(50) NOT NULL,
	rol ENUM ('admin', 'user')
);

CREATE TABLE IF NOT EXISTS GROUPS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50),
	creationDate DATE
);

CREATE TABLE IF NOT EXISTS USER_GROUP(
	id_user INT UNSIGNED,
	id_group INT UNSIGNED
);

CREATE TABLE IF NOT EXISTS ALBUMS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50),
	recordIndustry VARCHAR(50),
	bandName VARCHAR(50),
	unitsSold INT DEFAULT 0,
	presentationDate DATE
);

CREATE TABLE IF NOT EXISTS SONGS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50),
	musicComposer VARCHAR(50),
	lyricsComposer VARCHAR(50),
	duration REAL default 0,
	releaseDate DATE,
	id_album INT UNSIGNED
);

CREATE TABLE IF NOT EXISTS VIDEOCLIPS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	cost REAL default 0,
	shootingDays INTEGER default 0,
	directorName VARCHAR(50),
	studioName VARCHAR(50),
	recordingDate DATE,
	id_song INT UNSIGNED
);


INSERT INTO USERS(username, password, rol) values('admin', 'admin', 'admin');
INSERT INTO USERS(username, password, rol) values('user', 'user' , 'user');