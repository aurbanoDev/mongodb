CREATE DATABASE IF NOT EXISTS music;
use music;

CREATE TABLE IF NOT EXISTS USERS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(50) NOT NULL,
	rol ENUM ('admin', 'user')
);

CREATE TABLE IF NOT EXISTS ALBUMS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50),
	recordIndustry VARCHAR(50),
	bandName VARCHAR(50),
	unitsSold INT DEFAULT 0,
	presentationDate DATE
);

CREATE TABLE IF NOT EXISTS SONGS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(50),
	musicComposer VARCHAR(50),
	lyricsComposer VARCHAR(50),
	duration REAL default 0,
	releaseDate DATE,
	id_album INT UNSIGNED,
	id_videoclip INT UNSIGNED
);

CREATE TABLE IF NOT EXISTS VIDEOCLIPS(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	cost REAL default 0,
	shootingDays INTEGER default 0,
	directorName VARCHAR(50),
	studioName VARCHAR(50),
	recordigDate DATE,
	id_song INT UNSIGNED
);

DELIMITER |
CREATE FUNCTION user_exists(p_login varchar(50)) returns boolean
	begin
		if p_login not in(select username from users) then
			return false;
		end if;
		
		return true;
	end;
|
DELIMITER ;

DELIMITER |
CREATE FUNCTION isAdmin(p_login varchar(50)) returns boolean
	begin
		declare role int;
		set role = (select count(*) FROM USERS WHERE username = p_login AND rol ='admin');
		
		if (role > 0) then
			return true;
		end if;
		
		return false;
	end;
|
DELIMITER ;

delimiter |
create PROCEDURE newUser(p_username varchar(50), p_password varchar(50))
	procedureNU : begin
		if user_exists(p_username) THEN
			leave procedureNU;
		end if;
		insert into users (username, password, rol) values(p_username, password(p_password), 'user');
	end procedureNU;
|
delimiter ;

delimiter |
create PROCEDURE deleteUser(p_username varchar(50))
	procedureDU : begin
		declare userCreated boolean;
		set userCreated = user_exists(p_username);
		
		if userCreated = false THEN
			leave procedureDU;
		end if;
		
		if isAdmin(p_username) THEN
			leave procedureDU;
		end if;
		
		DELETE FROM users WHERE username = p_username;
		
	end procedureDU;
|
delimiter ;

INSERT INTO USERS(username, password, rol) values('admin', password('admin') , 'admin');
INSERT INTO USERS(username, password, rol) values('user', password('user') , 'user');
INSERT INTO ALBUMS(name, recordIndustry, bandName, unitsSold, presentationDate) values('noAlbum','','', 0 , null);
INSERT INTO SONGS(name, musicComposer, lyricsComposer, duration, releaseDate, id_videoclip, id_album) values('noSong', '', '', 0, null, 1, 1);
INSERT INTO VIDEOCLIPS(cost, shootingDays, directorName, studioName, recordigDate, id_song) values(0, 0, 'noClip', '', null, 1);

