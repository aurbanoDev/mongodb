package com.aurbano.musicManagement.dataAccessObjects;

import com.aurbano.musicManagement.music.Song;
import com.aurbano.musicManagement.music.VideoClip;
import com.aurbano.musicManagement.utils.Utils;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseClipDAO implements GenericDAO<VideoClip> {

    private MongoDatabase dataBase;

    public DataBaseClipDAO(MongoDatabase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public void add(VideoClip clip) {
        Document document = new Document()
                .append("directorName", clip.getDirectorName())
                .append("shootingDays", String.valueOf(clip.getShootingDays()))
                .append("studioName", clip.getStudioName())
                .append("cost", String.valueOf(clip.getCost()))
                .append("id_song", clip.getId_song())
                .append("recordingDate", Utils.dateFormat(clip.getRecordingDate()));

        dataBase.getCollection(VideoClip.COLLECTION).insertOne(document);
    }

    @Override
    public void update(VideoClip clip) {
        dataBase.getCollection(VideoClip.COLLECTION).replaceOne(new Document
                ("_id", clip.getId()), new Document()
                .append("directorName", clip.getDirectorName())
                .append("shootingDays", String.valueOf(clip.getShootingDays()))
                .append("studioName", clip.getStudioName())
                .append("cost", String.valueOf(clip.getCost()))
                .append("id_song", clip.getId_song())
                .append("recordingDate", Utils.dateFormat(clip.getRecordingDate())));
    }

    @Override
    public void delete(VideoClip item) {
        dataBase.getCollection(VideoClip.COLLECTION).deleteOne(new Document("_id", item.getId()));
    }

    @Override
    public List<VideoClip> getList() {
        FindIterable<Document> findIterable = dataBase.getCollection(VideoClip.COLLECTION).find();

        return getClipList(findIterable);
    }

    private List<VideoClip> getClipList(FindIterable<Document> findIterable) {
        List<VideoClip> clips = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        VideoClip videoClip = null;
        while (iterator.hasNext()) {
            Document document = iterator.next();
            videoClip = new VideoClip();
            videoClip.setId(document.getObjectId("_id"));
            videoClip.setDirectorName(document.getString("directorName"));
            videoClip.setStudioName(document.getString("studioName"));
            videoClip.setShootingDays(Integer.parseInt(document.getString("shootingDays")));
            videoClip.setCost(Float.parseFloat(document.getString("cost")));
            videoClip.setRecordingDate(Utils.dateParse(document.getString("recordingDate")));
            videoClip.setId_song(document.getObjectId("id_song"));
            clips.add(videoClip);
        }

        return clips;
    }

    public VideoClip getClip(ObjectId id) {
        FindIterable<Document> findIterable = dataBase.getCollection(VideoClip.COLLECTION).find(new Document("_id", id));
        Document document = findIterable.first();

        if (document == null)
            return null;

        VideoClip videoClip = new VideoClip();
        videoClip.setId(document.getObjectId("_id"));
        videoClip.setDirectorName(document.getString("directorName"));
        videoClip.setStudioName(document.getString("studioName"));
        videoClip.setShootingDays(Integer.parseInt(document.getString("shootingDays")));
        videoClip.setCost(Float.parseFloat(document.getString("cost")));
        videoClip.setRecordingDate(Utils.dateParse(document.getString("recordingDate")));
        videoClip.setId_song(document.getObjectId("id_song"));

        return videoClip;
    }

    public VideoClip getClipBySong(Song song) {
        FindIterable<Document> iter = dataBase.getCollection(VideoClip.COLLECTION).find(new Document("id_song", song.getId()));
        Document document = iter.first();

        if (document == null)
            return null;

        VideoClip videoClip = new VideoClip();
        videoClip.setId(document.getObjectId("_id"));
        videoClip.setDirectorName(document.getString("directorName"));
        videoClip.setStudioName(document.getString("studioName"));
        videoClip.setShootingDays(Integer.parseInt(document.getString("shootingDays")));
        videoClip.setCost(Float.parseFloat(document.getString("cost")));
        videoClip.setRecordingDate(Utils.dateParse(document.getString("recordingDate")));
        videoClip.setId_song(document.getObjectId("id_song"));

        return videoClip;
    }
}
