package com.aurbano.musicManagement.dataAccessObjects;

import com.mongodb.client.MongoDatabase;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseDAOfactory {

    private MongoDatabase dataBase;

    public DataBaseDAOfactory(MongoDatabase database) {
        this.dataBase = database;
    }

    public DataBaseSongDAO getSongDAO() {
        return new DataBaseSongDAO(dataBase);
    }

    public DataBaseAlbumDAO getAlbumDAO() {
        return new DataBaseAlbumDAO(dataBase);
    }

    public DataBaseClipDAO getClipDAO() {
        return new DataBaseClipDAO(dataBase);
    }
}
