package com.aurbano.musicManagement.dataAccessObjects;

import java.util.List;

/**
 * Interfaz gen�rica que define las operaciones b�sicas de persistencia.
 * @Author Adrian Urbano
 */
public interface GenericDAO<T> {

    void add(T item);
    void update(T item);
    void delete(T item);

    List<T> getList();
}
