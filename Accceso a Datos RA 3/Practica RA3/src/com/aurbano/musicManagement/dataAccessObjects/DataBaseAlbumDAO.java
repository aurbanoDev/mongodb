package com.aurbano.musicManagement.dataAccessObjects;

import com.aurbano.musicManagement.music.Album;
import com.aurbano.musicManagement.utils.Utils;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseAlbumDAO implements GenericDAO<Album> {

    private MongoDatabase dataBase;

    public DataBaseAlbumDAO(MongoDatabase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public void add(Album album) {
        Document documento = new Document()
                .append("name", album.getName())
                .append("bandName", album.getBandName())
                .append("recordIndustry", album.getRecordIndustry())
                .append("unitsSold", String.valueOf(album.getUnitsSold()))
                .append("presentationDate", Utils.dateFormat(album.getPresentationDate()));

        dataBase.getCollection(Album.COLLECTION).insertOne(documento);
    }

    @Override
    public void update(Album album) {
        dataBase.getCollection(Album.COLLECTION).replaceOne(new Document
                ("_id", album.getId()), new Document()
                .append("name", album.getName())
                .append("bandName", album.getBandName())
                .append("recordIndustry", album.getRecordIndustry())
                .append("unitsSold", String.valueOf(album.getUnitsSold()))
                .append("presentationDate", Utils.dateFormat(album.getPresentationDate())));
    }


    @Override
    public void delete(Album item) {
        dataBase.getCollection(Album.COLLECTION).deleteOne(new Document("_id", item.getId()));
    }

    @Override
    public List<Album> getList() {
        FindIterable<Document> findIterable = dataBase.getCollection(Album.COLLECTION).find();

        return getAlbumList(findIterable);
    }

    private List<Album> getAlbumList(FindIterable<Document> findIterable) {

        List<Album> albums = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        Album album = null;
        while (iterator.hasNext()) {
            Document document = iterator.next();
            album = new Album();
            album.setId(document.getObjectId("_id"));
            album.setName(document.getString("name"));
            album.setRecordIndustry(document.getString("recordIndustry"));
            album.setBandName(document.getString("bandName"));
            album.setUnitsSold(Integer.parseInt(document.getString("unitsSold")));
            album.setPresentationDate(Utils.dateParse(document.getString("presentationDate")));
            albums.add(album);
        }

        return albums;
    }

    public Album getAlbum(ObjectId id) {
        FindIterable<Document> findIterable = dataBase.getCollection(Album.COLLECTION).find(new Document("_id", id));
        Document document = findIterable.first();
        if (document == null)
            return null;

        Album album = new Album();
        album.setId(document.getObjectId("_id"));
        album.setName(document.getString("name"));
        album.setRecordIndustry(document.getString("recordIndustry"));
        album.setBandName(document.getString("bandName"));
        album.setUnitsSold(Integer.parseInt(document.getString("unitsSold")));
        album.setPresentationDate(Utils.dateParse(document.getString("presentationDate")));

        return album;
    }
}
