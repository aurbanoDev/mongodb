package com.aurbano.musicManagement.dataAccessObjects;

import com.aurbano.musicManagement.music.Album;
import com.aurbano.musicManagement.music.Song;
import com.aurbano.musicManagement.utils.Utils;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseSongDAO implements GenericDAO<Song> {

    private MongoDatabase dataBase;

    public DataBaseSongDAO(MongoDatabase dataBase) {
        this.dataBase = dataBase;
    }

    @Override
    public void add(Song song) {
        Document documento = new Document()
                .append("name", song.getName())
                .append("musicComposer", song.getMusicComposer())
                .append("lyrics", song.getLyricsComposer())
                .append("duration", String.valueOf(song.getDuration()))
                .append("releaseDate", Utils.dateFormat(song.getReleaseDate()))
                .append("id_album", song.getId_album());

        dataBase.getCollection(Song.COLLECTION).insertOne(documento);
    }

    @Override
    public void update(Song song) {
        dataBase.getCollection(Song.COLLECTION).replaceOne(new Document
                ("_id", song.getId()), new Document()
                .append("name", song.getName())
                .append("musicComposer", song.getMusicComposer())
                .append("lyrics", song.getLyricsComposer())
                .append("duration", String.valueOf(song.getDuration()))
                .append("id_album", song.getId_album())
                .append("releaseDate", Utils.dateFormat(song.getReleaseDate())));
    }

    @Override
    public void delete(Song item) {
        dataBase.getCollection(Song.COLLECTION).deleteOne(new Document("_id", item.getId()));
    }

    @Override
    public ArrayList<Song> getList() {
        FindIterable<Document> findIterable = dataBase.getCollection(Song.COLLECTION).find();

        return getSongList(findIterable);
    }

    private ArrayList<Song> getSongList(FindIterable<Document> findIterable) {

        ArrayList<Song> songs = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();

        while (iterator.hasNext()) {
            Document document = iterator.next();
            Song song = new Song();
            song.setId(document.getObjectId("_id"));
            song.setName(document.getString("name"));
            song.setMusicComposer(document.getString("musicComposer"));
            song.setLyricsComposer(document.getString("lyrics"));
            song.setDuration(Float.parseFloat(document.getString("duration")));
            song.setReleaseDate(Utils.dateParse(document.getString("releaseDate")));
            song.setId_album(document.getObjectId("id_album"));
            songs.add(song);
        }

        return songs;
    }

    public ArrayList<Song> getsongListByAlbum(Album album) {
        FindIterable<Document> findIterable = dataBase.getCollection(Song.COLLECTION).find(
                new Document("id_album", album.getId()));

        return getSongList(findIterable);
    }


    public Song getSong(ObjectId id) {
        FindIterable<Document> findIterable = dataBase.getCollection(Song.COLLECTION).find(new Document("_id", id));
        Document document = findIterable.first();

        if (document == null)
            return null;

        Song song = new Song();
        song.setId(document.getObjectId("_id"));
        song.setName(document.getString("name"));
        song.setMusicComposer(document.getString("musicComposer"));
        song.setLyricsComposer(document.getString("lyrics"));
        song.setDuration(Float.parseFloat(document.getString("duration")));
        song.setReleaseDate(Utils.dateParse(document.getString("releaseDate")));
        song.setId_album(document.getObjectId("id_album"));

        return song;
    }
}
