package com.aurbano.musicManagement.app;

import java.io.File;

/**
 * Conjunto de constantes utilizadas en la aplicacion
 * @author Adrian Urbano
 */
public class Constants {
    /**
     * Indexado del JTabbedPane
     */
    public static final int ALBUM_TAB = 0;
    public static final int SONG_TAB = 1;
    public static final int CLIP_TAB = 2;

    public static final String PROPERTIES_PATH = System.getProperty("user.home") + File.separator + "config.prop";

    /**
     * Cabeceras
     */
    public static final String[] ALBUM_HEADER =
            {"ID", "NAME", "R. INDUSTRY", "BAND NAME", "UNITS SOLD", "P. DATE"};
    public static final String[] CLIP_HEADER =
            {"ID", "COST", "SHOOTING DAYS", "DIRECTOR NAME", "STUDIO NAME", "RECORDING DATE"};
    public static final String[] SONG_HEADER =
            {"ID", "NAME", "MUSIC COMPOSER", "LYRICS COMPOSER", "DURATION", "RELEASE DATE"};


    public static final String DATA_BASE = "music";
}
