package com.aurbano.musicManagement.app;

import com.aurbano.musicManagement.dataAccessObjects.DataBaseAlbumDAO;
import com.aurbano.musicManagement.dataAccessObjects.DataBaseClipDAO;
import com.aurbano.musicManagement.dataAccessObjects.DataBaseDAOfactory;
import com.aurbano.musicManagement.dataAccessObjects.DataBaseSongDAO;
import com.aurbano.musicManagement.utils.DataBase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import static com.aurbano.musicManagement.app.Constants.PROPERTIES_PATH;

/**
 * Modelo de la aplicacion, se encarga de gestionar las operaciones de persistencia.
 * @author Adrian Urbano
 */
public class MusicModel {

    private DataBase dataBase;
    private DataBaseAlbumDAO albumDAO;
    private DataBaseClipDAO clipDAO;
    private DataBaseSongDAO songDAO;


    public MusicModel() {
        File props = new File(PROPERTIES_PATH);
        if (!props.exists())
            createPropertiesFile();

        dataBase = new DataBase();
        dataBase.startConnection();

        DataBaseDAOfactory factory = new DataBaseDAOfactory(dataBase.getDataBase());
        albumDAO = factory.getAlbumDAO();
        songDAO = factory.getSongDAO();
        clipDAO = factory.getClipDAO();
    }

    public DataBaseAlbumDAO getAlbumDAO() {
        return albumDAO;
    }

    public DataBaseClipDAO getClipDAO() {
        return clipDAO;
    }

    public DataBaseSongDAO getSongDAO() {
        return songDAO;
    }

    public DataBase getDataBase() {
        return dataBase;
    }

    private void createPropertiesFile() {
        Properties properties = new Properties();

        try {
            properties.setProperty("user", "root");
            properties.setProperty("pasword", "");

            properties.store(new FileOutputStream(PROPERTIES_PATH), "config");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
