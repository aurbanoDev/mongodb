package com.aurbano.musicManagement.app;

import com.aurbano.musicManagement.utils.Utils;

import static com.aurbano.musicManagement.app.Constants.*;

/**
 * Clase encargada de evaluar la entrada de datos de la vista. Funciona como apoyo para la clase
 * controller de la aplicaci�n.
 * @see MusicController
 * @author Adrian Urbano
 */
public class ViewDataHandler {

    private MusicView view;

    public ViewDataHandler(MusicView view) {
        this.view = view;
    }

    /**
     * Devuelve true si los datos introducidos en los JTexfields son v�lidos, en otro caso false.
     * @return boolean
     */
    public boolean isReliableEntryDATA() {
        int tabIndex = view.tabs.getSelectedIndex();

        switch (tabIndex) {
            case ALBUM_TAB:
                return isReliableEntryDataForALBUM();
            case SONG_TAB:
                return isReliableEntryDataForSONG();
            case CLIP_TAB:
                return isReliableEntryDataForCLIP();
            default:
                return false;
        }
    }

    /**
     *Devuelve true si los datos introducidos en los JTexfields son v�lidos para un album, en otro caso false.
     * @return boolean
     */
    private boolean isReliableEntryDataForALBUM() {

        if (view.tfNameAlbum.getText().equals("")) {
            Utils.showErrorDialog("The field Name can't be empty", "Illegal entry data");
            return false;
        }

        if ( !(Utils.isNumber(view.tfUnitsSoldAlbum.getText())))  {
            Utils.showDialog("Wrong Input, a number expected on the field : Units Sold ");
            return false;
        }

        return true;
    }

    /**
     *Devuelve true si los datos introducidos en los JTexfields son v�lidos para una cancion, en otro caso false.
     * @return boolean
     */
    private boolean isReliableEntryDataForSONG() {

        if (view.tfNmeSong.getText().equals("")) {
            Utils.showErrorDialog("The field Name can't be empty", "Illegal entry data");
            return false;
        }

        if ( !(Utils.isNumber(view.tfDurationSong.getText())))  {
            Utils.showDialog("Wrong Input, a number expected on the field : Duration ");
            return false;
        }

        if (view.cbAlbum_SongTab.getSelectedIndex() == -1) {
            Utils.showErrorDialog("Choose a album", "");
            return false;
        }

        return true;
    }

    /**
     *Devuelve true si los datos introducidos en los JTexfields son v�lidos para un Videoclip, en otro caso false.
     * @return boolean
     */
    private boolean isReliableEntryDataForCLIP() {

        if (view.tfDirectorName.getText().equals("")) {
            Utils.showErrorDialog("The field Director's Name can't be empty", "Illegal entry data");
            return false;
        }

        if ( !(Utils.isNumber(view.tfCost.getText())))  {
            Utils.showDialog("Wrong Input, a number expected on the field : Cost ");
            return false;
        }

        if (!(Utils.isNumber(view.tfShootingDays.getText())))  {
            Utils.showDialog("Wrong Input, a number expected on the field : Shooting Days ");
            return false;
        }

        return true;
    }

    /**
     * Selecciona el primer elemento de la tabla si no esta vacio.
     */
    public void loadFirstAlbum() {
         if (view.albumsTable.getRowCount() > 1) {
             view.albumsTable.setRowSelectionInterval(0, 0);
             return;
         }

        view.tfNameAlbum.setText("");
        view.tfBandNameAlbum.setText("");
        view.tfRecordIndustryAlbum.setText("");
        view.tfUnitsSoldAlbum.setText("");
        view.presentationDate_Album.setDate(null);
    }

    public void loadFirstSong() {
        if (view.songsTable.getRowCount() > 1) {
            view.songsTable.setRowSelectionInterval(0, 0);
            return;
        }

        view.tfNmeSong.setText("");
        view.tfDurationSong.setText("");
        view.tfLyricComposer.setText("");
        view.tfMusicComposer.setText("");
        view.releaseDate_Song.setDate(null);
    }

    public void loadFirstClip() {
        if (view.clipsTable.getRowCount() > 1) {
            view.clipsTable.setRowSelectionInterval(0, 0);
            return;
        }

        view.tfDirectorName.setText("");
        view.tfStudioName.setText("");
        view.tfShootingDays.setText("");
        view.tfCost.setText("");
        view.recordingDate_Clip.setDate(null);
    }
}
