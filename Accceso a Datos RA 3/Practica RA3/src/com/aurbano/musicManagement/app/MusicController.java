package com.aurbano.musicManagement.app;

import com.aurbano.musicManagement.music.*;
import com.aurbano.musicManagement.utils.Utils;
import org.bson.types.ObjectId;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.aurbano.musicManagement.app.Constants.*;

/**
 * Controlador de la aplicaci�n, clase encargada de comunicar la vista con el modelo.
 * @author Adrian Urbano
 */
public class MusicController extends KeyAdapter implements ActionListener, ListSelectionListener {

    private MusicModel model;
    private MusicView view;
    private ViewDataHandler viewDataHandler;


    public MusicController(MusicModel model, MusicView view) {
        this.model = model;
        this.view = view;
        viewDataHandler = new ViewDataHandler(view);

        setListeners();
        initialLoadState();

        refreshAllTables();
        refreshAllComboBox();
        view.loginDialog.showLogin();
    }

    private void setListeners() {
        view.buttonsAlbum.setListeners(this);
        view.buttonsClip.setListeners(this);
        view.buttonsSong.setListeners(this);
        view.tfSearch_album.addKeyListener(this);
        view.tfSearch_clip.addKeyListener(this);
        view.tfSearch_song.addKeyListener(this);
        view.menuBar.setListeners(this);
        view.loginDialog.setListeners(this);
        view.deleteUserDialog.setListener(this);
        view.createUserDialog.setListeners(this);
        view.albumsTable.setListener(this);
        view.songsTable.setListener(this);
        view.clipsTable.setListener(this);
    }

    private void initialLoadState() {
        setViewMode(ALBUM_TAB);
        setViewMode(SONG_TAB);
        setViewMode(CLIP_TAB);
    }

    private void refreshAllTables() {
        view.albumsTable.refresh(model.getAlbumDAO().getList());
        view.songsTable.refresh(model.getSongDAO().getList());
        view.clipsTable.refresh(model.getClipDAO().getList());
    }

    private void refreshAllComboBox() {
        view.cbAlbum_SongTab.refreshList(model.getAlbumDAO().getList());
        view.cbClips_SongTab.refreshList(model.getClipDAO().getList());
        view.cbSongs_ClipTab.refreshList(model.getSongDAO().getList());

        view.cbAlbum_SongTab.setSelectedIndex(-1);
        view.cbClips_SongTab.setSelectedIndex(-1);
        view.cbSongs_ClipTab.setSelectedIndex(-1);
        view.cbSongsAlbumTab.setSelectedIndex(-1);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = ((AbstractButton) e.getSource()).getActionCommand();
        Object source = e.getSource();

        if (source instanceof JMenuItem)
            menuItems_ActionPerformed(actionCommand);

        if (source instanceof JButton)
            crudButtons_ActionPerformed(actionCommand);
    }

    private void menuItems_ActionPerformed(String actionCommand) {

        switch (actionCommand) {
            case "newAlbum":
                view.tabs.setSelectedIndex(ALBUM_TAB);
                setSignUpMode();
                break;
            case "newSong":
                view.tabs.setSelectedIndex(SONG_TAB);
                setSignUpMode();
                break;
            case "newVideoClip":
                view.tabs.setSelectedIndex(CLIP_TAB);
                setSignUpMode();
                break;
            case "userManagement":
                view.createUserDialog.refreshGroupList(model.getDataBase().getGroups());
                view.createUserDialog.refreshUserList(model.getDataBase().getUsers());
                view.createUserDialog.setAdminMode(true);
                view.createUserDialog.setVisible(true);
                break;
            case "refresh":
                autoRefresh();
                break;
            case "closeSession":
                closeSession();
                break;
            case "importXML":
                File xml = Utils.getSelectedFile();
                if (xml == null)
                    return;

                List<Album> xmlList = Utils.getAlbumFromXml(xml.getPath());
                for (Album album : xmlList)
                    model.getAlbumDAO().add(album);

                refreshAllTables();
                break;
            case "exportXML":
                File folder = Utils.getSelectedDirectory();
                if (folder == null)
                    return;

                Utils.exportAlbumsAsXML(model.getAlbumDAO().getList(), folder.getPath());
                break;
            case "exit":
               System.exit(0);
            default:
                System.out.println("no deberia llegar aqui");
        }
    }

    private void closeSession() {
        clearInputFields();
        view.menuBar.enableAdminMode(false);
        view.createUserDialog.clearInputfields();
        view.loginDialog.setVisible(true);
    }

    private void autoRefresh() {
        Runnable refresh = new Runnable() {
            public void run() {

                refreshAllTables();
                String time = String.valueOf(new GregorianCalendar().getTime()).substring(11, 20);
                view.lbStatus.setText("  Last Refresh: " + time );
            }
        };

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(refresh, 0, 10, TimeUnit.SECONDS);
    }

    /**
     * Bifurca la logica de los eventos de los botones y guarda los cambios si el autosave e   sta activo.
     * @param actionCommand del boton que disparo el evento
     */
    private void crudButtons_ActionPerformed(String actionCommand) {

        switch (actionCommand) {
            case "save":
                saveButton_ActionPerformed();
                break;
            case "edit":
                editButton_ActionPerformed();
                break;
            case "delete":
                deleteButton_ActionPerformed();
                break;
            case "cancel":
                cancelButton_ActionPerformed();
                break;
            case "signUp":
                signUpButton_ActionPerformed();
                break;
            case "saveUserGroup":
                saveUserGroupButton_ActionPerformed();
                break;
            case "cancelUserGroup":
                view.createUserDialog.setViewMode();
                break;
            case "newUG":
                view.createUserDialog.clearInputfields();
                view.createUserDialog.setEditableMode(true);
                break;
            case "deleteUG":
                okDeleteUserButton_ActionPerformed();
                break;
            case "editUG":
                editUGButton_ActionPerformed();
                break;
            case "okdialog":
                okLoginButton_ActionPerformed();
                break;
            case "okDeleteUser":
                okDeleteUserButton_ActionPerformed();
                break;
            case "canceldialog":
                System.exit(0);
            default:
                System.out.println("no deberia llegar aqui");
        }

    }

    private void signUpButton_ActionPerformed() {
        view.createUserDialog.refreshGroupList(model.getDataBase().getGroups());
        view.createUserDialog.refreshUserList(model.getDataBase().getUsers());
        view.createUserDialog.setAdminMode(false);
        view.createUserDialog.setViewMode();
        view.createUserDialog.setVisible(true);
    }

    private void editUGButton_ActionPerformed() {
        int index = view.createUserDialog.getCurrentIndex();

        switch (index) {
            case 0:
                if (view.createUserDialog.getSelectedUser() == null) {
                    Utils.showErrorDialog("The Selection is Empty", "alert");
                    return;
                }

                break;
            case 1:
                if (view.createUserDialog.getSelectedGroup_groupTab() == null) {
                    Utils.showErrorDialog("The Selection is Empty", "alert");
                    return;
                }

                break;
        }


        view.createUserDialog.setEditMode(true);
        view.createUserDialog.setEditableMode(true);
    }

    private void saveUserGroupButton_ActionPerformed() {
        int index = view.createUserDialog.getCurrentIndex();

        switch (index) {
            case 0:
                newUserButton_ActionPerformed();
                break;
            case 1:
                newGroupButton_ActionPerformed();
                break;
        }

        view.createUserDialog.refreshGroupList(model.getDataBase().getGroups());
        view.createUserDialog.refreshUserList(model.getDataBase().getUsers());

        if (view.createUserDialog.isEditMode())
            view.createUserDialog.setEditableMode(false);

        view.createUserDialog.setEditMode(false);
    }

    /**
     * Metodo que controla el acceso a los metodos de guardado
     * notifica el la JLabel de estado.
     */
    private void saveButton_ActionPerformed() {
        if ( !viewDataHandler.isReliableEntryDATA())
            return;

        int tabIndex = view.tabs.getSelectedIndex();
        switch (tabIndex) {
            case ALBUM_TAB:
                saveButtonAlbum();
                view.albumsTable.refresh(model.getAlbumDAO().getList());
                break;
            case SONG_TAB:
                saveButtonSong();
                view.songsTable.refresh(model.getSongDAO().getList());
                break;
            case CLIP_TAB:
                saveButtonClip();
                view.clipsTable.refresh(model.getClipDAO().getList());
                break;
        }

        if (view.editionMode) {
            setViewMode();
            view.editionMode = false;
        }

        clearInputFields();
        refreshAllComboBox();
    }

    public void saveButtonAlbum() {
        Album album = getInputAlbum();

        if (!view.editionMode) {
            model.getAlbumDAO().add(album);
            return;
        }

        model.getAlbumDAO().update(album);
    }

    public void saveButtonSong() {
        Song song = getInputSong();

        if (!view.editionMode) {
            model.getSongDAO().add(song);
            return;
        }

        model.getSongDAO().update(song);
    }

    public void saveButtonClip() {
        VideoClip clip = getInputClip();

        if (!view.editionMode) {
            model.getClipDAO().add(clip);
            return;
        }

        model.getClipDAO().update(clip);
    }

    /**
     * Metodo que controla el acceso de los paneles del JTabbedPane al modo edicion.
     */
    private void editButton_ActionPerformed() {
        int tabIndex = view.tabs.getSelectedIndex();

        switch (tabIndex) {
            case ALBUM_TAB:
                if (view.albumsTable.getSelectedItemID() == null) {
                    Utils.showErrorDialog("The Selection is Empty", "alert");
                    return;
                }

                break;
            case SONG_TAB:
                if (view.songsTable.getSelectedItemID() == null) {
                    Utils.showErrorDialog("The selection of Song is empty", "alert");
                    return;
                }

                break;
            case CLIP_TAB:
                if (view.clipsTable.getSelectedItemID() == null) {
                    Utils.showErrorDialog("The selection of Clip is empty", "alert");
                    return;
                }

                break;
        }

        refreshAllComboBox();
        setEditMode();
        view.editionMode = true;
    }

    /**
     * Metodo que controla el acceso a los metodos de borrado
     * notifica el la JLabel de estado.
     */
    private void deleteButton_ActionPerformed() {
        int tabIndex = view.tabs.getSelectedIndex();

        switch (tabIndex) {
            case ALBUM_TAB:

                 if (view.albumsTable.getSelectedItemID() == null) {
                     Utils.showErrorDialog("The Selection is Empty", "alert");
                     return;
                 }

                if (JOptionPane.showConfirmDialog(null, " Are you sure ?") != 0)
                    return;

                albumDeleteButton_ActionPerformed();
                view.albumsTable.refresh(model.getAlbumDAO().getList());
                break;
            case SONG_TAB:
                if (view.songsTable.getSelectedItemID() == null) {
                    Utils.showErrorDialog("The Selection is Empty", "alert");
                    return;
                }

                if (JOptionPane.showConfirmDialog(null, "Are you sure ?") != 0)
                    return;

                songDeleteButton_ActionPerformed();
                view.songsTable.refresh(model.getSongDAO().getList());
                break;
            case CLIP_TAB:
                if (view.clipsTable.getSelectedItemID() == null) {
                    Utils.showErrorDialog("The Selection is Empty", "alert");
                    return;
                }

                if (JOptionPane.showConfirmDialog(null, "Are you sure ?") != 0)
                    return;

                clipDeleteButton_ActionPerformed();
                view.clipsTable.refresh(model.getClipDAO().getList());
                break;
            default:
                System.out.println("no deberia llegar aqui");
        }

        view.albumsTable.refresh(model.getAlbumDAO().getList());
        //refreshAllTables();
        refreshAllComboBox();
}

    private void albumDeleteButton_ActionPerformed() {
        ObjectId id = view.albumsTable.getSelectedItemID();
        Album disposableAlbum = model.getAlbumDAO().getAlbum(id);

        model.getAlbumDAO().delete(disposableAlbum);
        viewDataHandler.loadFirstAlbum();
    }

    private void songDeleteButton_ActionPerformed() {
        ObjectId id = view.songsTable.getSelectedItemID();
        Song disposableSong = model.getSongDAO().getSong(id);
        model.getSongDAO().delete(disposableSong);
        viewDataHandler.loadFirstSong();

        if (model.getClipDAO().getList().isEmpty())
            clearInputFields(CLIP_TAB);
    }

    private void clipDeleteButton_ActionPerformed() {
        ObjectId id = view.clipsTable.getSelectedItemID();
        VideoClip disposableClip = model.getClipDAO().getClip(id);
        viewDataHandler.loadFirstClip();

        model.getClipDAO().delete(disposableClip);
        viewDataHandler.loadFirstClip();
    }

    private void cancelButton_ActionPerformed() {
        clearInputFields();
        setViewMode();
    }

    private void newUserButton_ActionPerformed() {
        User user = new User();
        if (view.createUserDialog.isEditMode())
            user = view.createUserDialog.getSelectedUser();

        String username = view.createUserDialog.getUsername();
        String password = view.createUserDialog.getPassword();

        user.setUsername(username);
        user.setPassword(password);

        Group group = view.createUserDialog.getSelectedGroup();
        if (group != null)
            user.setId_group(group.getId());


        view.createUserDialog.clearInputfields();

        if (model.getDataBase().userExists(username) && !view.createUserDialog.isEditMode()) {
            Utils.showErrorDialog("The user already exists", "");
            return;
        }

        if (view.createUserDialog.isEditMode()) {
            model.getDataBase().updateUser(user);
            return;
        }

        model.getDataBase().saveUser(user);
    }


    private void newGroupButton_ActionPerformed() {
        Group group = new Group();

        if (view.createUserDialog.isEditMode())
            group = view.createUserDialog.getSelectedGroup_groupTab();

        String groupName = view.createUserDialog.getGroupName();
        Date creationDate = view.createUserDialog.getCreationDate();

        group.setName(groupName);
        group.setCreationDate(creationDate);

        view.createUserDialog.clearInputfields();

        if (view.createUserDialog.isEditMode()) {
            model.getDataBase().updateGroup(group);
            return;
        }

        model.getDataBase().saveGroup(group);
    }

    private void okLoginButton_ActionPerformed() {
        String login = view.loginDialog.getTfLogin().getText();
        String password = view.loginDialog.getTfPassword().getText();

        boolean successfulLogin = model.getDataBase().checkLogin(login, password);
        if (!successfulLogin) {
            Utils.showErrorDialog("Login or Password Failed", "");
            return;
        }

        view.loginDialog.clearFields();
        view.loginDialog.dispose();

        String role = model.getDataBase().getUserRole(login);
        view.lbStatus.setText(role);

        if (role.equalsIgnoreCase("admin"))
            view.menuBar.enableAdminMode(true);
    }

    private void okDeleteUserButton_ActionPerformed() {
        if (JOptionPane.showConfirmDialog(null, "Are you sure ?") != 0)
            return;

        if (view.createUserDialog.getCurrentIndex() == 0) {
            User user = view.createUserDialog.getSelectedUser();
            model.getDataBase().deleteUser(user.getUsername());
        }

        if (view.createUserDialog.getCurrentIndex() == 1) {
            Group group = view.createUserDialog.getSelectedGroup_groupTab();
            model.getDataBase().deleteGroup(group.getName());

        }

        view.createUserDialog.refreshUserList(model.getDataBase().getUsers());
        view.createUserDialog.refreshGroupList(model.getDataBase().getGroups());
        view.createUserDialog.setViewMode();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        int tabIndex = view.tabs.getSelectedIndex();

        if (tabIndex == ALBUM_TAB)
            albumTab_ValueChanged();

        if (tabIndex == SONG_TAB)
            songTab_ValueChanged();

        if (tabIndex == CLIP_TAB)
            clipTab_ValueChanged();

        if (e.getSource() == view.createUserDialog.getGroupListGroupTab())
            groups_ValueChanged(e);

        if (e.getSource() == view.createUserDialog.getUserListUserTab())
            users_ValueChanged();
    }

    private void users_ValueChanged() {
        User user = view.createUserDialog.getUserListUserTab().getSelectedValue();

        if (user == null)
            return;

        view.createUserDialog.loadUserData(user);
    }

    private void groups_ValueChanged(ListSelectionEvent e) {
        Group group = view.createUserDialog.getGroupListGroupTab().getSelectedValue();
        if (group == null)
            return;

        view.createUserDialog.loadGroupData(group);
    }

    public void albumTab_ValueChanged() {
        ObjectId id = view.albumsTable.getSelectedItemID();
        if (id == null)
            return;

        Album album = model.getAlbumDAO().getAlbum(id);
        if (album == null) {
            clearInputFields();
            return;
        }

        loadAlbumData(album);
    }

    private void songTab_ValueChanged() {
        ObjectId id = view.songsTable.getSelectedItemID();
        if (id == null)
            return;

        Song song = model.getSongDAO().getSong(id);
        if (song == null) {
            clearInputFields();
            return;
        }

        loadSongData(song);
    }

    private void clipTab_ValueChanged() {
        ObjectId id = view.clipsTable.getSelectedItemID();
        if (id == null)
            return;

        VideoClip clip = model.getClipDAO().getClip(id);
        if (clip == null) {
            clearInputFields();
            return;
        }

        loadClipData(clip);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        JTextField source = (JTextField) e.getSource();

        if (source == view.tfSearch_album)
            albumTab_KeyReleased();

        if (source == view.tfSearch_song)
            songTab_KeyReleased();

        if (source == view.tfSearch_clip)
            clipTab_KeyReleased();
    }

    /**
     * Metodos que utilizan el Key event del JTexfield para buscar en la lista.
     * Se obtienen los diferentes criterios de busqueda a traves del Combobox
     */
    private void albumTab_KeyReleased() {

        List<Album> tempList = new ArrayList<>();
        String pattern = view.tfSearch_album.getText();
        String criterion = view.cbSearch_Album.getSelectedItem();

        switch (criterion) {
            case "Name":
                for (Album album : model.getAlbumDAO().getList()) {
                    if (String.valueOf(album.getName()).startsWith(pattern))
                        tempList.add(album);
                }
                break;
            case "Record Industry":
                for (Album album : model.getAlbumDAO().getList()) {
                    if (String.valueOf(album.getRecordIndustry()).startsWith(pattern))
                        tempList.add(album);
                }
                break;
            case "Band Name":
                for (Album album : model.getAlbumDAO().getList()) {
                    if (album.getBandName().startsWith(pattern))
                        tempList.add(album);
                }
                break;
            case "Units Sold":
                for (Album album : model.getAlbumDAO().getList()) {
                    if (String.valueOf(album.getUnitsSold()).startsWith(pattern))
                        tempList.add(album);
                }
                break;
        }

        view.albumsTable.refresh(tempList);
    }

    private void songTab_KeyReleased() {

        List<Song> tempList = new ArrayList<>();;
        String pattern = view.tfSearch_song.getText();
        String criterion = view.cbSearch_Song.getSelectedItem();

        switch (criterion) {
            case "Name":
                for (Song song : model.getSongDAO().getList()) {
                    if (song.getName().startsWith(view.tfSearch_song.getText()))
                        tempList.add(song);
                }
                break;
            case "Music Composer":
                for (Song song : model.getSongDAO().getList()) {
                    if (song.getMusicComposer().startsWith(view.tfSearch_song.getText()))
                        tempList.add(song);
                }
               break;
            case "Lyrics Composer":
                for (Song song : model.getSongDAO().getList()) {
                    if (song.getLyricsComposer().startsWith(view.tfSearch_song.getText()))
                        tempList.add(song);
                }
                break;
            case "Duration":
                for (Song song : model.getSongDAO().getList()) {
                    if (String.valueOf(song.getDuration()).startsWith(view.tfSearch_song.getText()))
                        tempList.add(song);
                }
                break;
        }

        view.songsTable.refresh(tempList);
    }

    private void clipTab_KeyReleased() {

        List<VideoClip> tempList = new ArrayList<>();
        String pattern = view.tfSearch_clip.getText();
        String criterion = view.cbSearch_Clip.getSelectedItem();

        switch (criterion) {
            case "Director Name":
                for (VideoClip clip : model.getClipDAO().getList()) {
                    if (clip.getDirectorName().startsWith(view.tfSearch_clip.getText()))
                        tempList.add(clip);
                }
                break;
            case "Studio Name":
                for (VideoClip clip : model.getClipDAO().getList()) {
                    if (clip.getStudioName().startsWith(view.tfSearch_clip.getText()))
                        tempList.add(clip);
                }
                break;
            case "Shooting Days":
                for (VideoClip clip : model.getClipDAO().getList()) {
                    if (String.valueOf(clip.getShootingDays()).startsWith(view.tfSearch_clip.getText()))
                        tempList.add(clip);
                }
                break;
            case "Cost":
                tempList = new ArrayList<>();
                for (VideoClip clip : model.getClipDAO().getList()) {
                    if (String.valueOf(clip.getCost()).startsWith(view.tfSearch_clip.getText()))
                        tempList.add(clip);
                }
                break;
        }

       view.clipsTable.refresh(tempList);
    }

    /**
     * Activa el modo dar de alta de panel del JTabbedPane que tiene el foco
     */
    public void setSignUpMode() {
        int index = view.tabs.getSelectedIndex();

        switch (index) {
            case ALBUM_TAB:
                view.buttonsAlbum.setSignUpMode();
                setInputComponentsState(true, view.inputPanelAlbum);
                view.cbSongsAlbumTab.setEnabled(false);
                view.albumsTable.setEnabled(false);
                break;
            case SONG_TAB:
                view.buttonsSong.setSignUpMode();
                setInputComponentsState(true, view.inputSongPanel);
                view.songsTable.setEnabled(false);
                break;
            case CLIP_TAB:
                view.buttonsClip.setSignUpMode();
                setInputComponentsState(true, view.inputClipPanel);
                view.clipsTable.setEnabled(false);
                break;
        }

        refreshAllComboBox();
        clearInputFields();
    }

    /**
     * Activa el modo vista de panel del JTabbedPane que tiene el foco.
     */
    private void setViewMode() {
        int index = view.tabs.getSelectedIndex();
        setViewMode(index);
    }

    /**
     * Activa el modo vista
     * @param index del panel del JTabbedPane que se quiere activar el modo vista
     */
    private void setViewMode(int index) {
        switch (index) {
            case ALBUM_TAB:
                view.buttonsAlbum.setEnabledState(false, true, true, false);
                setInputComponentsState(false, view.inputPanelAlbum);
                view.cbSongsAlbumTab.setEnabled(true);
                view.albumsTable.setEnabled(true);
                view.cbSongsAlbumTab.clearSelection();
                view.albumsTable.clearSelection();
                break;
            case SONG_TAB:
                view.buttonsSong.setEnabledState(false, true, true, false);
                setInputComponentsState(false, view.inputSongPanel);
                view.songsTable.setEnabled(true);
                view.songsTable.clearSelection();
                view.cbClips_SongTab.clearSelection();
                break;
            case CLIP_TAB:
                view.buttonsClip.setEnabledState(false, true, true, false);
                setInputComponentsState(false, view.inputClipPanel);
                view.clipsTable.setEnabled(true);
                view.clipsTable.clearSelection();
                view.cbSongs_ClipTab.clearSelection();
                break;
        }
    }

    /**
     * Activa el modo Edicion del panel del tabbedPane que tenga el foco
     */
    public void setEditMode() {
        int index = view.tabs.getSelectedIndex();
        setEditMode(index);
    }

    /**
     * Activa el modo Edicion
     * @param index del panel del JTabbedPane que se quiere activar el modo edici�n
     */
    public void setEditMode(int index) {
        switch (index) {
            case ALBUM_TAB:
                view.buttonsAlbum.setEnabledState(true, false, false, true);
                setInputComponentsState(true, view.inputPanelAlbum);
                view.albumsTable.setEnabled(false);
                break;
            case SONG_TAB:
                view.buttonsSong.setEnabledState(true, false, false, true);
                setInputComponentsState(true, view.inputSongPanel);
                 view.songsTable.setEnabled(false);
                break;
            case CLIP_TAB:
                view.buttonsClip.setEnabledState(true, false, false, true);
                setInputComponentsState(true, view.inputClipPanel);
                view.clipsTable.setEnabled(false);
                break;

        }
    }

    /**
     * Metodo para modificar el estado (enabled , editable) de los componentes de un Jpanel.
     * @param enabled true o false
     * @param inputPanel el JPanel en el que buscar� los componentes.
     */
    private void setInputComponentsState(boolean enabled, JPanel inputPanel) {
        for (Component component : inputPanel.getComponents())
            if (component instanceof JTextField)
                ((JTextField) component).setEditable(enabled);
            else
                component.setEnabled(enabled);
    }

    /**
     * Borra el texto de los Jtexfields del panel que tenga el foco del tabbedpane
     */
    public void clearInputFields() {
        int index = view.tabs.getSelectedIndex();
        clearInputFields(index);
    }

    public void clearInputFields(int index) {
        switch (index) {
            case ALBUM_TAB:
                view.tfNameAlbum.setText("");
                view.tfBandNameAlbum.setText("");
                view.tfRecordIndustryAlbum.setText("");
                view.tfUnitsSoldAlbum.setText("");
                view.presentationDate_Album.setDate(null);
                break;
            case SONG_TAB:
                view.tfNmeSong.setText("");
                view.tfDurationSong.setText("");
                view.tfLyricComposer.setText("");
                view.tfMusicComposer.setText("");
                view.releaseDate_Song.setDate(null);
                break;
            case CLIP_TAB:
                view.tfDirectorName.setText("");
                view.tfStudioName.setText("");
                view.tfShootingDays.setText("");
                view.tfCost.setText("");
                view.recordingDate_Clip.setDate(null);
                break;
        }
    }

    public void loadAlbumData(Album album) {
        view.tfNameAlbum.setText(album.getName());
        view.tfBandNameAlbum.setText(album.getBandName());
        view.tfRecordIndustryAlbum.setText(album.getRecordIndustry());
        view.tfUnitsSoldAlbum.setText(String.valueOf(album.getUnitsSold()));
        view.presentationDate_Album.setDate(album.getPresentationDate());

        view.cbSongsAlbumTab.refreshList(model.getSongDAO().getsongListByAlbum(album));
    }

    /**
     * Visualiza los datos de una Cancion  a traves de los componentes de la vista.
     * @param song de la cual se van a visualizar los datos.
     */
    public void loadSongData(Song song) {
        view.tfNmeSong.setText(song.getName());
        view.tfMusicComposer.setText(song.getMusicComposer());
        view.tfLyricComposer.setText(song.getLyricsComposer());
        view.tfDurationSong.setText(String.valueOf(song.getDuration()));
        view.releaseDate_Song.setDate(song.getReleaseDate());

        view.cbAlbum_SongTab.clearSelection();
        view.cbClips_SongTab.clearSelection();

        if (song.getId_album() != null) {
            Album album = model.getAlbumDAO().getAlbum(song.getId_album());
            System.out.println(album);
            view.cbAlbum_SongTab.addItem(album);
            view.cbAlbum_SongTab.setSelectedItem(album);
        }

        VideoClip clip = model.getClipDAO().getClipBySong(song);
        if (clip != null) {
            System.out.println(clip);
            view.cbClips_SongTab.addItem(clip);
            view.cbClips_SongTab.setSelectedItem(clip);
        }

    }

    /**
     * Visualiza los datos de un VideoClip  a traves de los componentes de la vista.
     * @param clip del cual se va a extraer los datos
     */
    public void loadClipData(VideoClip clip) {
        view.tfDirectorName.setText(clip.getDirectorName());
        view.tfStudioName.setText(clip.getStudioName());
        view.tfShootingDays.setText(String.valueOf(clip.getShootingDays()));
        view.tfCost.setText(String.valueOf(clip.getCost()));
        view.recordingDate_Clip.setDate(clip.getRecordingDate());

        view.cbSongs_ClipTab.clearSelection();

        if (clip.getId_song() != null) {
            Song song = model.getSongDAO().getSong(clip.getId_song());
            System.out.println(song);
            view.cbSongs_ClipTab.addItem(song);
            view.cbSongs_ClipTab.setSelectedItem(song);
        }
    }


    /**
     * Metodo que contruye un Album a partir de los datos extraidos de la vista,
     * o modificando los datos de otro Album.
     * @return Album
     */
    public Album getInputAlbum() {
        Album album = new Album();
        if (view.editionMode) {
            ObjectId id = view.albumsTable.getSelectedItemID();
            album = model.getAlbumDAO().getAlbum(id);
        }

        if (view.tfUnitsSoldAlbum.getText().equals(""))
            view.tfUnitsSoldAlbum.setText("0");

        if (view.presentationDate_Album != null)
            album.setPresentationDate(view.presentationDate_Album.getDate());

        album.setName(view.tfNameAlbum.getText());
        album.setBandName(view.tfBandNameAlbum.getText());
        album.setRecordIndustry(view.tfRecordIndustryAlbum.getText());
        album.setUnitsSold(Integer.parseInt(view.tfUnitsSoldAlbum.getText()));

        return album;
    }

    /**
     * Metodo que contruye una Cancion a partir de los datos extraidos de la vista,
     * o modificando los datos de otra Cancion.
     * @return Song
     */

    public Song getInputSong() {
        Song song = new Song();
        if (view.editionMode) {
            ObjectId id = view.songsTable.getSelectedItemID();
            song = model.getSongDAO().getSong(id);
        }

        if (view.tfDurationSong.getText().equals(""))
            view.tfDurationSong.setText("0");

        if (view.releaseDate_Song.getDate() != null)
            song.setReleaseDate(view.releaseDate_Song.getDate());

        if (view.cbAlbum_SongTab.getSelectedItem() != null)
           song.setId_album(view.cbAlbum_SongTab.getSelectedItem().getId());

        song.setName(view.tfNmeSong.getText());
        song.setLyricsComposer(view.tfLyricComposer.getText());
        song.setMusicComposer(view.tfMusicComposer.getText());
        song.setDuration(Float.parseFloat(view.tfDurationSong.getText()));

        return song;
    }

    /**
     * Metodo que contruye un VideoClip a partir de los datos extraidos de la vista,
     * o modificando los datos de otro VideoClip.
     * @return VideoClip
     */
    public VideoClip getInputClip() {
        VideoClip clip = new VideoClip();
        if (view.editionMode) {
            ObjectId id = view.clipsTable.getSelectedItemID();
            clip = model.getClipDAO().getClip(id);
        }

        if (view.tfCost.getText().equals(""))
            view.tfCost.setText("0");

        if (view.tfShootingDays.getText().equals(""))
            view.tfShootingDays.setText("0");

        if (view.recordingDate_Clip.getDate() != null)
            clip.setRecordingDate(view.recordingDate_Clip.getDate());

        if (view.cbSongs_ClipTab.getSelectedItem() != null)
            clip.setId_song(view.cbSongs_ClipTab.getSelectedItem().getId());

        clip.setStudioName(view.tfStudioName.getText());
        clip.setDirectorName(view.tfDirectorName.getText());
        clip.setCost(Float.parseFloat(view.tfCost.getText()));
        clip.setShootingDays(Integer.parseInt(view.tfShootingDays.getText()));

        return clip;
    }
}
