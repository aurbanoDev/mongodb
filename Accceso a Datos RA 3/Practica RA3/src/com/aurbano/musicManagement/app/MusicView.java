package com.aurbano.musicManagement.app;

import com.aurbano.musicManagement.componentsGUI.*;
import com.aurbano.musicManagement.componentsGUI.combos.MyComboBox;
import com.aurbano.musicManagement.componentsGUI.tables.AlbumTable;
import com.aurbano.musicManagement.componentsGUI.tables.ClipTable;
import com.aurbano.musicManagement.componentsGUI.tables.SongTable;
import com.aurbano.musicManagement.music.Album;
import com.aurbano.musicManagement.music.Song;
import com.aurbano.musicManagement.music.VideoClip;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * La vista de la aplicaci�n.
 * @author Adrian Urbano
 */
public class MusicView {

    ButtonsPanel buttonsAlbum;
    ButtonsPanel buttonsClip;
    ButtonsPanel buttonsSong;

    JTextField tfSearch_song;
    JTextField tfBandNameAlbum;
    JTextField tfRecordIndustryAlbum;
    JTextField tfNameAlbum;
    JTextField tfUnitsSoldAlbum;
    JTextField tfNmeSong;
    JTextField tfMusicComposer;
    JTextField tfLyricComposer;
    JTextField tfDurationSong;
    JTextField tfSearch_clip;
    JTextField tfDirectorName;
    JTextField tfStudioName;
    JTextField tfCost;
    JTextField tfShootingDays;
    JTextField tfSearch_album;

    JLabel lbStatus;
    boolean editionMode;
    MyMenuBar menuBar;
    JPanel inputPanelAlbum;
    JPanel inputClipPanel;
    JPanel inputSongPanel;

    MyComboBox<String> cbSearch_Song;
    MyComboBox<String> cbSearch_Clip;
    MyComboBox<String> cbSearch_Album;

    MyComboBox<Song> cbSongsAlbumTab;
    MyComboBox<Album> cbAlbum_SongTab;
    MyComboBox<VideoClip> cbClips_SongTab;
    MyComboBox<Song> cbSongs_ClipTab;

    JDateChooser recordingDate_Clip;
    JDateChooser releaseDate_Song;
    JDateChooser presentationDate_Album;

    LoginDialog loginDialog;
    UserDeleteDialog deleteUserDialog;
    CreateUserDialog createUserDialog;

    AlbumTable albumsTable;
    SongTable songsTable;
    ClipTable clipsTable;

    JTabbedPane tabs;
    private JPanel panel1;
    private JPanel albumTab;
    private JPanel bottomPanel;
    private JPanel topPanel;
    private JPanel centerPanel;
    private JPanel ScrollPanel;
    private JScrollPane jscrollPane;
    private JPanel SongTab;
    private JPanel songTop;
    private JPanel botSongPanel;
    private JPanel centerSongPanel;
    private JPanel songScrollPanel;
    private JPanel VideoClipTab;
    private JPanel clipBottom;
    private JPanel clipTop;
    private JPanel clipCenter;
    private JFrame frame;


    public MusicView() {
        menuBar = new MyMenuBar();
        loginDialog = new LoginDialog();
        deleteUserDialog = new UserDeleteDialog();
        createUserDialog = new CreateUserDialog();

        frame = new JFrame("MusicView");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(menuBar);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

        setSearchValues();
        setPrototypeDisplayValues();
    }

    private void setSearchValues() {
        cbSearch_Album.loadComboBoxValues(
                "<Select Field for Search>", "Name", "Record Industry", "Band Name", "Units Sold");

        cbSearch_Song.loadComboBoxValues(
                "<Select Field for Search>", "Name", "Music Composer", "Lyrics Composer", "Duration");

        cbSearch_Clip.loadComboBoxValues(
                "<Select Field for Search>", "Director Name", "Studio Name", "Shooting Days", "Cost");
    }

    private void setPrototypeDisplayValues() {
        cbAlbum_SongTab.setPrototypeDisplayValue(new Album());
        cbClips_SongTab.setPrototypeDisplayValue(new VideoClip());
        cbSongsAlbumTab.setPrototypeDisplayValue(new Song());
        cbSongs_ClipTab.setPrototypeDisplayValue(new Song());
    }

}
