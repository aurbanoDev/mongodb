package com.aurbano.musicManagement;

import com.aurbano.musicManagement.app.MusicController;
import com.aurbano.musicManagement.app.MusicModel;
import com.aurbano.musicManagement.app.MusicView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by adriu on 05/11/2015.
 */
public class Main {

    public static void main(String [] args) {

        try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MusicView view = new MusicView();
                MusicModel model = new MusicModel();
                MusicController controller = new MusicController(model, view);
            }
        });

    }
}
