package com.aurbano.musicManagement.componentsGUI;

import com.aurbano.musicManagement.music.User;

import javax.swing.*;
import java.awt.event.*;
import java.util.List;

public class UserDeleteDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel central;
    private JScrollPane scrollpanel;
    private JList<String> list1;
    private DefaultListModel<String> model;

    public UserDeleteDialog() {
        model = new DefaultListModel<>();
        list1.setModel(model);
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        initCancelButton();
        setLocationRelativeTo(null);
        setModal(true);
        setTitle("List of Users");
        pack();
    }

    public void setListener(ActionListener listener) {
        buttonOK.addActionListener(listener);
        buttonOK.setActionCommand("okDeleteUser");
    }

    public void showDialog() {
        setVisible(true);
    }

    public User getSelectedUser() {

        return null;
    }

    public void loadUsers(List<User> users) {
        model.removeAllElements();
        for (User user : users)
            model.addElement(user.getUsername());
    }

    private void onCancel() {
        dispose();
    }

    private void initCancelButton() {
        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }
}
