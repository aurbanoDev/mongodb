package com.aurbano.musicManagement.componentsGUI.combos;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Combo box
 * @author Adrian Urbano
 */
public class MyComboBox<E> extends JComboBox<E> {

    protected List<E> items;
    protected E selectedItem;

    public MyComboBox() {
        super();
        items = new ArrayList<>();
    }

    public void setItems(List<E> items) {
        this.items = items;
    }

    public E getSelectedItem() {
        selectedItem = getModel().getElementAt(getSelectedIndex());
        return selectedItem;
    }

    public void refreshList() {
        removeAllItems();

        for (E e : items) {
            addItem(e);
        }
    }

    public void refreshList(List<E> list) {
        removeAllItems();
        setItems(list);

        for (E e : items) {
            addItem(e);
        }
    }

    public void clearSelection() {
        setSelectedIndex(-1);
    }

    public void loadComboBoxValues(E... args) {
        for (E value : args) {
            items.add(value);
        }
        refreshList();
    }

    public List<E> getItems() {
        return items;
    }

}
