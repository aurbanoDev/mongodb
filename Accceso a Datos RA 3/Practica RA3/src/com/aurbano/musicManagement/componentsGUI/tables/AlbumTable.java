package com.aurbano.musicManagement.componentsGUI.tables;

import com.aurbano.musicManagement.music.Album;
import org.bson.types.ObjectId;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.util.List;

import static com.aurbano.musicManagement.app.Constants.ALBUM_HEADER;

/**
 * Created by adriu on 06/12/2015.
 */
public class AlbumTable extends JTable {

    private DefaultTableModel model;

    public AlbumTable() {
        model = new DefaultTableModel(ALBUM_HEADER, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };
        setModel(model);
    }

    public void setListener(ListSelectionListener listener) {
        getSelectionModel().addListSelectionListener(listener);
    }

    public ObjectId getSelectedItemID() {
        if (getSelectedRow() != -1)
            return (ObjectId) getValueAt(getSelectedRow(), 0);

        return null;
    }

    public void clear() {
        model.setRowCount(0);
    }

    public void refresh(List<Album> albums) {
        model.setRowCount(0);

        for (Album album : albums) {
           Object[] row = new Object[] {
                   album.getId(),
                   album.getName(),
                   album.getRecordIndustry(),
                   album.getBandName(),
                   album.getUnitsSold(),
                   album.getPresentationDate()
           };

            if (!row[1].equals("noAlbum"))
                model.addRow(row);
        }
    }
}
