package com.aurbano.musicManagement.componentsGUI.tables;

import com.aurbano.musicManagement.music.Song;
import org.bson.types.ObjectId;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.util.List;

import static com.aurbano.musicManagement.app.Constants.SONG_HEADER;

/**
 * Created by adriu on 07/12/2015.
 */
public class SongTable extends JTable {

    private DefaultTableModel model;

    public SongTable() {
        model = new DefaultTableModel(SONG_HEADER, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        setModel(model);
    }

    public void setListener(ListSelectionListener listener) {
        getSelectionModel().addListSelectionListener(listener);
    }

    public ObjectId getSelectedItemID() {
        if (getSelectedRow() != -1)
            return (ObjectId) getValueAt(getSelectedRow(), 0);

        return null;
    }

    public void clear() {
        model.setRowCount(0);
    }

    public void refresh(List<Song> songs) {
        model.setRowCount(0);

        for (Song song : songs) {
            Object[] row = new Object[] {
                    song.getId(),
                    song.getName(),
                    song.getMusicComposer(),
                    song.getLyricsComposer(),
                    song.getDuration(),
                    song.getReleaseDate()
            };

            if (!row[1].equals("noSong"))
                model.addRow(row);
        }
    }

}
