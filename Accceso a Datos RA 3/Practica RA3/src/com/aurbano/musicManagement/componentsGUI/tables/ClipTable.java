package com.aurbano.musicManagement.componentsGUI.tables;

import com.aurbano.musicManagement.music.VideoClip;
import org.bson.types.ObjectId;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.util.List;

import static com.aurbano.musicManagement.app.Constants.CLIP_HEADER;

/**
 * Created by adriu on 07/12/2015.
 */
public class ClipTable extends JTable {

    private DefaultTableModel model;

    public ClipTable() {
        model = new DefaultTableModel(CLIP_HEADER, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        setModel(model);
    }

    public void setListener(ListSelectionListener listener) {
        getSelectionModel().addListSelectionListener(listener);
    }

    public ObjectId getSelectedItemID() {
        if (getSelectedRow() != -1)
            return (ObjectId) getValueAt(getSelectedRow(), 0);

        return null;
    }

    public void clear() {
        model.setRowCount(0);
    }

    public void refresh(List<VideoClip> clips) {
        model.setRowCount(0);

        for (VideoClip clip : clips) {
            Object[] row = new Object[] {
                    clip.getId(),
                    clip.getCost(),
                    clip.getShootingDays(),
                    clip.getDirectorName(),
                    clip.getStudioName(),
                    clip.getRecordingDate()
            };

            if (!row[3].equals("noClip"))
                model.addRow(row);
        }
    }
}
