package com.aurbano.musicManagement.componentsGUI;

import javax.swing.*;
import java.awt.event.ActionListener;

public class LoginDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfLogin;
    private JTextField tfPassword;
    private JButton btnewUser;

    public LoginDialog() {
        setContentPane(contentPane);
        setModal(true);
        setUndecorated(true);
        setLocationRelativeTo(null);
        pack();
    }

    public void setListeners(ActionListener listener) {
        buttonOK.addActionListener(listener);
        buttonCancel.addActionListener(listener);
        btnewUser.addActionListener(listener);
    }

    public JTextField getTfLogin() {
        return tfLogin;
    }

    public JTextField getTfPassword() {
        return tfPassword;
    }

    public void clearFields() {
        tfPassword.setText("");
        tfLogin.setText("");
    }

    public void showLogin() {
        btnewUser.setEnabled(true);
        setVisible(true);
    }
}
