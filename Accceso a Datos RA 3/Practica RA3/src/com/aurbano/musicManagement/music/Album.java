package com.aurbano.musicManagement.music;

import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que representa un Album de m�sica.
 * @author  Adrian Urbano
 */


public class Album implements Serializable {


    public static final String COLLECTION = "albums";

    private ObjectId id;
    private String name;
    private String recordIndustry;
    private String bandName;
    private int unitsSold;
    private Date presentationDate;


    public Album() {
        this("", "", "", 0, new Date());
    }

    public Album(String name, String recordIndustry, String bandName, int unitsSold, Date presentationDate) {
        this.name = name;
        this.recordIndustry = recordIndustry;
        this.bandName = bandName;
        this.unitsSold = unitsSold;
        this.presentationDate = presentationDate;
    }

    public ObjectId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRecordIndustry() {
        return recordIndustry;
    }

    public String getBandName() {
        return bandName;
    }

    public Date getPresentationDate() {
        return presentationDate;
    }

    public int getUnitsSold() {
        return unitsSold;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecordIndustry(String recordIndustry) {
        this.recordIndustry = recordIndustry;
    }

    public void setBandName(String bandName) {
        this.bandName = bandName;
    }

    public void setPresentationDate(Date presentationDate) {
        this.presentationDate = presentationDate;
    }

    public void setUnitsSold(int unitsSold) {
        this.unitsSold = unitsSold;
    }

    @Override
    public String toString() {
        return name;
    }

}
