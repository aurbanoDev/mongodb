package com.aurbano.musicManagement.music;

import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

/**
 * Created by adriu on 05/02/2016.
 */

public class Group {

    public static final String COLLECTION = "groups";

    private ObjectId id;
    private String name;
    private Date creationDate;

    public Group() {
        this("", new Date());
    }

    public Group(String name, Date creationDate) {
        this.name = name;
        this.creationDate = creationDate;
    }

    public Group(String groupName, Date creationDate, List<User> users) {
        this.name = groupName;
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }
}
