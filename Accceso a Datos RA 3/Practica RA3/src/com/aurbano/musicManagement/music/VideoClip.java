package com.aurbano.musicManagement.music;


import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que representa un VideoClip.
 * @author  Adrian Urbano
 */

public class VideoClip implements Serializable {

    public static final String COLLECTION = "videoclips";

    private ObjectId id;
    private ObjectId id_song;

    private float cost;
    private int shootingDays;
    private String directorName;
    private String studioName;
    private Date recordingDate;

    public VideoClip() {
        this(0, 0, "", "", new Date());
    }

    public VideoClip(float cost, int shootingDays, String directorName, String studioName, Date recordingDate) {
        this.cost = cost;
        this.shootingDays = shootingDays;
        this.directorName = directorName;
        this.studioName = studioName;
        this.recordingDate = recordingDate;
    }

    public Date getRecordingDate() {
        return recordingDate;
    }

    public String getStudioName() {
        return studioName;
    }

    public String getDirectorName() {
        return directorName;
    }

    public int getShootingDays() {
        return shootingDays;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setShootingDays(int shootingDays) {
        this.shootingDays = shootingDays;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public void setRecordingDate(Date recordingDate) {
        this.recordingDate = recordingDate;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getId_song() {
        return id_song;
    }

    public void setId_song(ObjectId id_song) {
        this.id_song = id_song;
    }

    @Override
    public String toString() {
        return  directorName;
    }
}
