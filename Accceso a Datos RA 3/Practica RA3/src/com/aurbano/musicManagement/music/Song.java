package com.aurbano.musicManagement.music;


import org.bson.types.ObjectId;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase que representa una Cancion.
 * @author  Adrian Urbano
 */

public class Song implements Serializable {

    public static final String COLLECTION = "songs";

    private ObjectId id;
    private ObjectId id_album;

    private String name;
    private String musicComposer;
    private String lyricsComposer;
    private float duration;
    private Date releaseDate;

    public Song() {
        this("", "", "", 0, new Date());
    }

    public Song(String name, String musicComposer, String lyricsComposer, float duration, Date releaseDate) {
        this.name = name;
        this.musicComposer = musicComposer;
        this.lyricsComposer = lyricsComposer;
        this.duration = duration;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public String getMusicComposer() {
        return musicComposer;
    }

    public String getLyricsComposer() {
        return lyricsComposer;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public float getDuration() {
        return duration;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMusicComposer(String musicComposer) {
        this.musicComposer = musicComposer;
    }

    public void setLyricsComposer(String lyricsComposer) {
        this.lyricsComposer = lyricsComposer;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getId_album() {
        return id_album;
    }

    public void setId_album(ObjectId id_album) {
        this.id_album = id_album;
    }

    @Override
    public String toString() {
        return name;
    }
}
