package com.aurbano.musicManagement.music;

import org.bson.types.ObjectId;

/**
 * Created by adriu on 01/02/2016.
 */

public class User {

    public static final String COLLECTION = "users";

    private ObjectId id;
    private ObjectId id_group;

    private String username;
    private String password;
    private String rol;

    public User() {
        this("", "");
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public ObjectId getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return username;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getId_group() {
        return id_group;
    }

    public void setId_group(ObjectId id_group) {
        this.id_group = id_group;
    }
}
