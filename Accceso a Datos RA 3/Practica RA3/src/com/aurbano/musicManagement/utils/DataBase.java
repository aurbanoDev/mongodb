package com.aurbano.musicManagement.utils;

import com.aurbano.musicManagement.music.Group;
import com.aurbano.musicManagement.music.User;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.*;

import static com.aurbano.musicManagement.app.Constants.DATA_BASE;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBase {

    private MongoClient mongoClient;
    private MongoDatabase dataBase;

    public void startConnection() {
        mongoClient = new MongoClient();
        dataBase = mongoClient.getDatabase(DATA_BASE);
    }

    public void disconnect() {
        mongoClient.close();
    }

    public boolean checkLogin(String username, String password) {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("password", password);

        FindIterable<Document> findIterable = dataBase.getCollection(User.COLLECTION).find(new Document(map));
        Document document = findIterable.first();

        if (document == null)
            return false;

        return true;
    }

    public List<User> getUsers() {
        FindIterable<Document> findIterable = dataBase.getCollection(User.COLLECTION).find();

        return getUserList(findIterable);
    }

    private List<User> getUserList(FindIterable<Document> findIterable) {

        List<User> users = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();
        User user = null;

        while (iterator.hasNext()) {
            Document document = iterator.next();
            user = new User();
            user.setId(document.getObjectId("_id"));
            user.setUsername(document.getString("username"));
            user.setPassword(document.getString("password"));
            user.setRol(document.getString("rol"));
            user.setId_group(document.getObjectId("id_group"));
            users.add(user);
        }

        return users;
    }

    public boolean userExists(String username) {
        FindIterable<Document> findIterable = dataBase.getCollection(User.COLLECTION).find(new Document("username", username));

        if (findIterable.first() == null)
            return false;

        return true;
    }


    public User getUser(String username) {
        FindIterable<Document> findIterable = dataBase.getCollection(User.COLLECTION).find(new Document("username", username));
        Document document = findIterable.first();

        return documentToUser(document);
    }

    private User documentToUser(Document document) {
        User user = new User();
        user.setId(document.getObjectId("_id"));
        user.setUsername(document.getString("username"));
        user.setPassword(document.getString("password"));
        user.setRol(document.getString("rol"));
        user.setId_group(document.getObjectId("id_group"));

        return user;
    }

    public String getUserRole(String username) {
        User user = getUser(username);

        return user.getRol();
    }


    public void saveUser(User user) {
        Document documento = new Document()
                .append("username", user.getUsername())
                .append("password", user.getPassword())
                .append("rol", "user")
                .append("id_group", user.getId_group());

        dataBase.getCollection(User.COLLECTION).insertOne(documento);
    }

    public void deleteUser(String username) {
        dataBase.getCollection(User.COLLECTION).deleteOne(new Document("username", username));
    }

    public void updateUser(User user) {
        dataBase.getCollection(User.COLLECTION).replaceOne(new Document
                ("_id", user.getId()), new Document()
                .append("username", user.getUsername())
                .append("password", user.getPassword())
                .append("rol", user.getRol())
                .append("id_group", user.getId_group()));
    }


    public List<Group> getGroups() {
        FindIterable<Document> findIterable = dataBase.getCollection(Group.COLLECTION).find();

        return getGroupList(findIterable);
    }

    private List<Group> getGroupList(FindIterable<Document> findIterable) {

        List<Group> groups = new ArrayList<>();
        Iterator<Document> iterator = findIterable.iterator();
        Group group = null;

        while (iterator.hasNext()) {
            Document documento = iterator.next();
            group = new Group();
            group.setId(documento.getObjectId("_id"));
            group.setName(documento.getString("name"));
            group.setCreationDate(Utils.dateParse(documento.getString("creationDate")));
            groups.add(group);
        }

        return groups;
    }

    public void  saveGroup(Group group) {
        Document documento = new Document()
                .append("name", group.getName())
                .append("creationDate", Utils.dateFormat(group.getCreationDate()));

        dataBase.getCollection(Group.COLLECTION).insertOne(documento);
    }

    public void deleteGroup(String group) {
        dataBase.getCollection(Group.COLLECTION).deleteOne(new Document("name", group));
    }

    public void updateGroup(Group group) {
        dataBase.getCollection(Group.COLLECTION).replaceOne(new Document
                ("_id", group.getId()), new Document()
                .append("name", group.getName())
                .append("creationDate", Utils.dateFormat(group.getCreationDate())));
    }

    public MongoDatabase getDataBase() {
        return dataBase;
    }
}

