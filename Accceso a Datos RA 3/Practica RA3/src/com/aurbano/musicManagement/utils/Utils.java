package com.aurbano.musicManagement.utils;

import com.aurbano.musicManagement.music.Album;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase de utilidades.
 * @author Adrian Urbano
 */
public class Utils {

    public static boolean isNumber(String text) {
        Pattern notNumber = Pattern.compile("[^0-9]+(\\.[^0-9][^0-9]?)?");
        Matcher matcher = notNumber.matcher(text);

        return (matcher.matches()) ?  false : true ;
    }

    public static void showDialog(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    public static void showErrorDialog(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Recupera una instancia con la representacion del directorio escogida por
     * el usuario mediante un control file chooser.
     *
     * @return File
     */
    public static File getSelectedDirectory() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle("Choose a directory");
        chooser.setApproveButtonText("Save as ..");

        if (chooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION)
            return null;

        return chooser.getSelectedFile();
    }

    /**
     * Recupera una instancia con la representacion del fichero escogida por el
     * usuario mediante un control file chooser.
     *
     * @return File
     */
    public static File getSelectedFile() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setDialogTitle("Choose a file");
        chooser.setApproveButtonText("Save as ..");

        if (chooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION)
            return null;

        return chooser.getSelectedFile();
    }

    /**
     * Formatea una fecha en formato 23:59:59
     *
     * @param date
     * @return String con la fecha formateada
     */
    public static String dateFormat(Date date) {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        return format.format(date);
    }

    /**
     * Parsea a Date una cadena de texto con el formato "HH:mm:ss".
     *
     * @param date
     * @return Date con la fecha parseada
     */
    public static Date dateParse(String date) {
        try {
            DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Crea un documento XML con la lista de VideoClips proporcionada.
     * @param list que sera exportada como documento XML
     */
    public static void exportAlbumsAsXML(List<Album> list, String path) {

        if (list.isEmpty()) {
            showErrorDialog("The list is empty", "alert");
            return;
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        Document document = null;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            document = dom.createDocument(null, "xml", null);

            Element root = document.createElement("albums");
            document.getDocumentElement().appendChild(root);

            Element clipNode = null, dataNode = null;
            Text text = null;

            for (Album album : list) {
                clipNode = document.createElement("album");
                root.appendChild(clipNode);

                dataNode = document.createElement("name");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(album.getName());
                dataNode.appendChild(text);

                dataNode = document.createElement("recordIndustry");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(album.getRecordIndustry());
                dataNode.appendChild(text);

                dataNode = document.createElement("bandName");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(String.valueOf(album.getBandName()));
                dataNode.appendChild(text);

                dataNode = document.createElement("unitsSold");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(String.valueOf(album.getUnitsSold()));
                dataNode.appendChild(text);

                dataNode = document.createElement("presentationDate");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(Utils.dateFormat(album.getPresentationDate()));
                dataNode.appendChild(text);

                Source source = new DOMSource(document);
                Result result = new StreamResult(new File(path + "/exportedAlbums.xml"));

                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.transform(source, result);
            }

        } catch (ParserConfigurationException pex) {

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public static ArrayList<Album> getAlbumFromXml(String path) {

        ArrayList<Album> listado = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            documento = builder.parse(new File(path));

            NodeList albums = documento.getElementsByTagName("album");
            for (int i = 0; i < albums.getLength(); i++) {
                Node album = albums.item(i);
                Element elemento = (Element) album;

                Album albumcico = new Album();

                albumcico.setName(elemento.getElementsByTagName("name").item(0).
                        getChildNodes().item(0).getNodeValue());

                albumcico.setRecordIndustry(elemento.getElementsByTagName("recordIndustry").item(0).
                        getChildNodes().item(0).getNodeValue());

                albumcico.setBandName(elemento.getElementsByTagName("bandName").item(0).
                        getChildNodes().item(0).getNodeValue());

                albumcico.setUnitsSold(Integer.parseInt(elemento.getElementsByTagName("unitsSold").item(0).
                        getChildNodes().item(0).getNodeValue()));

                String fecha = elemento.getElementsByTagName("presentationDate").item(0).
                        getChildNodes().item(0).getNodeValue();

                albumcico.setPresentationDate(getSqlDateFromString(fecha));

                listado.add(albumcico);
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException saxe) {
            saxe.printStackTrace();
        }

        return listado;
    }

    public static java.sql.Date getDateSql(java.util.Date fecha) {
        return new java.sql.Date(fecha.getTime());
    }

    public static java.sql.Date getSqlDateFromString(String fecha) {

        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");

        try {
            return getDateSql(formatoDelTexto.parse(fecha));

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return null;
    }



}






